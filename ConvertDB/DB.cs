﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using DataModels;
using SQLite;

namespace ConvertDB
{
    class DB
    {
        private  XmlDocument doc = new XmlDocument();
        public SQLiteConnection trueDb = new SQLiteConnection("truedb.db", SQLiteOpenFlags.ReadOnly | SQLiteOpenFlags.NoMutex | SQLiteOpenFlags.ProtectionNone | SQLiteOpenFlags.SharedCache);
        public XmlNode res = null;
        private List<int> locLvls = new List<int>() { 0, 3, 5, 8, 11, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 35, 38, 0};
        public void InitDatabase()
        {
            doc.Load("xml.xml");
            res = doc.DocumentElement;
            using (var db = GetWritableDatabase())
            {
                db.CreateTable<AreaLure>();
                db.CreateTable<Bait>();
                db.CreateTable<Fish>();
                db.CreateTable<FishAreaLure>();
                db.CreateTable<FishBaitChance>();
                db.CreateTable<FishingHooks>();
                db.CreateTable<FishingLine>();
                db.CreateTable<FishSpinningChance>();
                db.CreateTable<FishSpinningSpeed>();
                db.CreateTable<FishTimeChance>();
                db.CreateTable<Location>();
                db.CreateTable<LocationFishWeightMult>();
                db.CreateTable<Reel>();
                db.CreateTable<Rod>();
                db.CreateTable<Spinning>();
                db.CreateTable<SpinningLure>();
                db.CreateTable<SpinningLureSpeed>();
            }
        }

        public SQLiteConnection GetReadableDatabase()
        {
            return new SQLiteConnection("db.db", SQLiteOpenFlags.ReadOnly | SQLiteOpenFlags.NoMutex | SQLiteOpenFlags.ProtectionNone | SQLiteOpenFlags.SharedCache);
        }

        public SQLiteConnection GetWritableDatabase()
        {
            return new SQLiteConnection("db.db", SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create | SQLiteOpenFlags.FullMutex | SQLiteOpenFlags.ProtectionComplete);
        }

        public void FillAreaLure()
        {

            var nameNodes = (from XmlNode node in res.ChildNodes
                             where node.Attributes["name"].Value == "prikorm_names"
                             select node).FirstOrDefault()?.ChildNodes;
            var names = from XmlNode node in nameNodes select node.InnerText;
            var priceNodes = (from XmlNode node in res.ChildNodes
                              where node.Attributes["name"].Value == "prikorm_prices"
                              select node).FirstOrDefault()?.ChildNodes;
            var prices = from XmlNode node in priceNodes select Int32.Parse(node.InnerText);
            var stackNodes = (from XmlNode node in res.ChildNodes
                             where node.Attributes["name"].Value == "prikorm_props"
                             select node).FirstOrDefault()?.ChildNodes;
            var stack = from XmlNode node in stackNodes select Int32.Parse(node.InnerText);
            var list = new List<AreaLure>();
            int i = 0;
            using (var e1 = names.GetEnumerator())
            using (var e2 = prices.GetEnumerator())
            using (var e3 = stack.GetEnumerator())
            {
                while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext())
                {
                    var item = new AreaLure();
                    item.ID = i++;
                    item.Name = e1.Current;
                    item.Price = e2.Current;
                    item.StackCount = e3.Current;
                    list.Add(item);
                }
            }
            var b = GetWritableDatabase();
            b.BeginTransaction();
            foreach (var item in list)
            {
                b.InsertOrReplace(item);
            }
            b.Commit();
            b.Close();
        }

        public void FillBait()
        {
            var nameNodes = (from XmlNode node in res.ChildNodes
                             where node.Attributes["name"].Value == "nazh_names"
                             select node).FirstOrDefault()?.ChildNodes;
            var names = from XmlNode node in nameNodes select node.InnerText;
            var priceNodes = (from XmlNode node in res.ChildNodes
                              where node.Attributes["name"].Value == "nazh_prices"
                              select node).FirstOrDefault()?.ChildNodes;
            var prices = from XmlNode node in priceNodes select Int32.Parse(node.InnerText);
            var stackNodes = (from XmlNode node in res.ChildNodes
                              where node.Attributes["name"].Value == "nazh_props"
                              select node).FirstOrDefault()?.ChildNodes;
            var stack = from XmlNode node in stackNodes select Int32.Parse(node.InnerText);
            var list = new List<Bait>();
            int i = 0;
            using (var e1 = names.GetEnumerator())
            using (var e2 = prices.GetEnumerator())
            using (var e3 = stack.GetEnumerator())
            {
                while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext())
                {
                    var item = new Bait();
                    item.ID = i++;
                    item.Name = e1.Current;
                    item.Price = e2.Current;
                    item.CountInStack = e3.Current;
                    list.Add(item);
                }
            }
            var b = GetWritableDatabase();
            b.BeginTransaction();
            foreach (var item in list)
            {
                b.InsertOrReplace(item);
            }
            b.Commit();
            b.Close();
        }

        public void FillFish()
        {
            var fishes = new List<Fish>();
            var res = trueDb.CustomQuery("select f.id, f.names_ru, f.fish_prices, f.davl, f.fish_trophy, f.temperatures, f.fish_exp, f.depth_start, f.depth_end from fishs f");
            foreach (var row in res)
            {
                var fish = new Fish();
                fish.ID = (int)row[0];
                fish.Name = (String)row[1] ?? "";
                fish.Price = (int)row[2];
                fish.Pressure = (int)row[3];
                fish.MinTrophyWeight = (int)row[4];
                fish.MinTemperature = (int)row[5];
                int tmp;
                Int32.TryParse((String) row[6], out tmp);
                fish.Expirience = tmp;
                fish.DepthMin = (int)row[7];
                fish.DepthMax = (int)row[8];
                fishes.Add(fish);
            }
            var b = GetWritableDatabase();
            b.BeginTransaction();
            foreach (var fish in fishes)
            {
                b.InsertOrReplace(fish);
            }
            b.Commit();
            b.Close();
        }

        public void FillFishAreaLure()
        {
            var list = new List<FishAreaLure>();
            int i = 0;
            var res = trueDb.CustomQuery("select f.id, f.prikorm_type from fishs f");
            foreach (var row in res)
            {
                var luresStr = (String) row[1] ?? "";
                var lures = luresStr.Split(',');
                foreach (var lure in lures)
                {
                    var fishAreaLure = new FishAreaLure();
                    fishAreaLure.UID = i++;
                    fishAreaLure.FishID = (int)row[0];
                    int lureid;
                    Int32.TryParse(lure, out lureid);
                    fishAreaLure.AreaLureID = lureid;
                    list.Add(fishAreaLure);
                }
            }
            var b = GetWritableDatabase();
            b.BeginTransaction();
            foreach (var item in list)
            {
                b.InsertOrReplace(item);
            }
            b.Commit();
            b.Close();
        }

        public void FillFishBaitChance()
        {
            var list = new List<FishBaitChance>();
            int i = 0;
            var res = trueDb.CustomQuery("select f.id, f.fish_nazh_id, f.fish_nazh_ch, f.fish_nazh_min, f.fish_nazh_max from fishs f");
            foreach (var row in res)
            {
                var lureidStr = (String) row[1];
                var lurechStr = (String)row[2];
                var lureminStr = (String)row[3];
                var luremaxStr = (String)row[4];
                var lureids = lureidStr.Split(',').ToList();
                var lurechs = lurechStr.Split(',').ToList();
                var luremin = lureminStr.Split(',').ToList();
                var luremax = luremaxStr.Split(',').ToList();
                using (var e1 = lureids.GetEnumerator())
                using (var e2 = lurechs.GetEnumerator())
                using (var e3 = luremin.GetEnumerator())
                using (var e4 = luremax.GetEnumerator())
                {
                    while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext() && e4.MoveNext())
                    {
                        var fishBaitChance = new FishBaitChance();
                        fishBaitChance.UID = i++;
                        fishBaitChance.FishID = (int) row[0];
                        int tmp;
                        Int32.TryParse(e1.Current, out tmp);
                        fishBaitChance.LureID = tmp;

                        Int32.TryParse(e2.Current, out tmp);
                        fishBaitChance.LureChance = tmp;

                        Int32.TryParse(e3.Current, out tmp);
                        fishBaitChance.LureMinWeight = tmp;

                        Int32.TryParse(e4.Current, out tmp);
                        fishBaitChance.LureMaxWeight = tmp;

                        list.Add(fishBaitChance);
                    }
                }
                    
            }
            var b = GetWritableDatabase();
            b.BeginTransaction();
            foreach (var item in list)
            {
                b.InsertOrReplace(item);
            }
            b.Commit();
            b.Close();
        }

        public void FillFishSpinningChance()
        {
            var list = new List<FishSpinningChance>();
            int i = 0;
            var res = trueDb.CustomQuery("select f.id, f.spin_id, f.spin_ch, f.spin_min_w, f.spin_max_w from fishs f");
            foreach (var row in res)
            {
                if (row[1] == null)
                    continue;
                var lureidStr = (String)row[1];
                var lurechStr = (String)row[2];
                var lureminStr = (String)row[3];
                var luremaxStr = (String)row[4];
                var lureids = lureidStr.Split(',').ToList();
                var lurechs = lurechStr.Split(',').ToList();
                var luremin = lureminStr.Split(',').ToList();
                var luremax = luremaxStr.Split(',').ToList();
                using (var e1 = lureids.GetEnumerator())
                using (var e2 = lurechs.GetEnumerator())
                using (var e3 = luremin.GetEnumerator())
                using (var e4 = luremax.GetEnumerator())
                {
                    while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext() && e4.MoveNext())
                    {
                        var fishSpinChance = new FishSpinningChance();

                        fishSpinChance.UID = i++;
                        fishSpinChance.FishID = (int)row[0];
                        int tmp;

                        Int32.TryParse(e1.Current, out tmp);
                        fishSpinChance.SpinningLureID = tmp;

                        Int32.TryParse(e2.Current, out tmp);
                        fishSpinChance.Chance = tmp;

                        Int32.TryParse(e3.Current, out tmp);
                        fishSpinChance.MinWeight = tmp;

                        Int32.TryParse(e4.Current, out tmp);
                        fishSpinChance.MaxWeight = tmp;

                        list.Add(fishSpinChance);
                    }
                }

            }
            var b = GetWritableDatabase();
            b.BeginTransaction();
            foreach (var item in list)
            {
                b.InsertOrReplace(item);
            }
            b.Commit();
            b.Close();
            
        }

        public void FillFishSpinningSpeed()
        {
            var list = new List<FishSpinningSpeed>();
            int i = 0;
            var res = trueDb.CustomQuery("select f.id, f.provodka from fishs f");
            foreach (var row in res)
            {
                if (row[1] == null)
                    continue;
                var luresStr = (String)row[1] ?? "";

                var fishSpinSpeed = new FishSpinningSpeed();
                fishSpinSpeed.ID = (int)row[0];

                fishSpinSpeed.SlowAllowed = luresStr.ToLower().Contains("slow");
                fishSpinSpeed.MediumAllowed = luresStr.ToLower().Contains("medium");
                fishSpinSpeed.FastAllowed = luresStr.ToLower().Contains("fast");
                    
                list.Add(fishSpinSpeed);
            }
            var b = GetWritableDatabase();
            b.BeginTransaction();
            foreach (var item in list)
            {
                b.InsertOrReplace(item);
            }
            b.Commit();
            b.Close();
        }

        public void FillFishTimeChance()
        {
            var list = new List<FishTimeChance>();
            var queryString = new StringBuilder("select f.id");
            for (int i = 0; i < 24; ++i)
            {
                queryString.Append(", f.time_" + i);
            }
            queryString.Append(" from fishs f");
            var res = trueDb.CustomQuery(queryString.ToString());
            foreach (var row in res)
            {
                var fishTimeChance = new FishTimeChance();
                fishTimeChance.ID = (int)row[0];
                fishTimeChance.TimeChance = String.Join(",", row.Select(i => i.ToString()).ToArray(), 1, 24);

                list.Add(fishTimeChance);
            }
            var b = GetWritableDatabase();
            b.BeginTransaction();
            foreach (var item in list)
            {
                b.InsertOrReplace(item);
            }
            b.Commit();
            b.Close();
            
        }

        public void FillFishingHooks()
        {
            var nameNodes = (from XmlNode node in res.ChildNodes
                             where node.Attributes["name"].Value == "cruk_names"
                             select node).FirstOrDefault()?.ChildNodes;
            var names = from XmlNode node in nameNodes select node.InnerText;
            var priceNodes = (from XmlNode node in res.ChildNodes
                              where node.Attributes["name"].Value == "cruk_prices"
                              select node).FirstOrDefault()?.ChildNodes;
            var prices = from XmlNode node in priceNodes select Int32.Parse(node.InnerText);
            var chanceNodes = (from XmlNode node in res.ChildNodes
                              where node.Attributes["name"].Value == "cruk_props"
                               select node).FirstOrDefault()?.ChildNodes;
            var chance = from XmlNode node in chanceNodes select Int32.Parse(node.InnerText);
            var list = new List<FishingHooks>();
            int i = 0;
            using (var e1 = names.GetEnumerator())
            using (var e2 = prices.GetEnumerator())
            using (var e3 = chance.GetEnumerator())
            {
                while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext())
                {
                    var item = new FishingHooks();
                    item.ID = i++;
                    item.Name = e1.Current;
                    item.Price = e2.Current;
                    item.Chance = e3.Current;
                    list.Add(item);
                }
            }
            var b = GetWritableDatabase();
            b.BeginTransaction();
            foreach (var item in list)
            {
                b.InsertOrReplace(item);
            }
            b.Commit();
            b.Close();
        }

        public void FillFishingLine()
        {
            var nameNodes = (from XmlNode node in res.ChildNodes
                             where node.Attributes["name"].Value == "les_names"
                             select node).FirstOrDefault()?.ChildNodes;
            var names = from XmlNode node in nameNodes select node.InnerText;
            var priceNodes = (from XmlNode node in res.ChildNodes
                              where node.Attributes["name"].Value == "les_prices"
                              select node).FirstOrDefault()?.ChildNodes;
            var prices = from XmlNode node in priceNodes select Int32.Parse(node.InnerText);
            var weightNodes = (from XmlNode node in res.ChildNodes
                               where node.Attributes["name"].Value == "les_props"
                               select node).FirstOrDefault()?.ChildNodes;
            var weight = from XmlNode node in weightNodes select Int32.Parse(node.InnerText);
            var list = new List<FishingLine>();
            int i = 0;
            using (var e1 = names.GetEnumerator())
            using (var e2 = prices.GetEnumerator())
            using (var e3 = weight.GetEnumerator())
            {
                while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext())
                {
                    var item = new FishingLine();
                    item.ID = i++;
                    item.Name = e1.Current;
                    item.Price = e2.Current;
                    item.MaxWeight = e3.Current;
                    list.Add(item);
                }
            }
            var b = GetWritableDatabase();
            b.BeginTransaction();
            foreach (var item in list)
            {
                b.InsertOrReplace(item);
            }
            b.Commit();
            b.Close();
        }
        
        public void FillLocation()
        {
            var nameNodes = (from XmlNode node in res.ChildNodes
                             where node.Attributes["name"].Value == "loc_names"
                             select node).FirstOrDefault()?.ChildNodes;
            var names = from XmlNode node in nameNodes select node.InnerText;
            var priceNodes = (from XmlNode node in res.ChildNodes
                              where node.Attributes["name"].Value == "loc_cost"
                              select node).FirstOrDefault()?.ChildNodes;
            var prices = from XmlNode node in priceNodes select Int32.Parse(node.InnerText);
            var minPresNodes = (from XmlNode node in res.ChildNodes
                               where node.Attributes["name"].Value == "min_pressures"
                                select node).FirstOrDefault()?.ChildNodes;
            var minPres = from XmlNode node in minPresNodes select Int32.Parse(node.InnerText);
            var minTmpNodes = (from XmlNode node in res.ChildNodes
                               where node.Attributes["name"].Value == "min_temps"
                               select node).FirstOrDefault()?.ChildNodes;
            var minTmp = from XmlNode node in minTmpNodes select Int32.Parse(node.InnerText);
            var maxTmpNodes = (from XmlNode node in res.ChildNodes
                               where node.Attributes["name"].Value == "max_temps"
                               select node).FirstOrDefault()?.ChildNodes;
            var maxTmp = from XmlNode node in maxTmpNodes select Int32.Parse(node.InnerText);
            var list = new List<Location>();
            int i = 0;
            using (var e1 = names.GetEnumerator())
            using (var e2 = prices.GetEnumerator())
            using (var e3 = locLvls.GetEnumerator())
            using (var e4 = minPres.GetEnumerator())
            using (var e5 = minTmp.GetEnumerator())
            using (var e6 = maxTmp.GetEnumerator())
            {
                while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext() && e4.MoveNext() && e5.MoveNext() && e6.MoveNext())
                {
                    var item = new Location();
                    item.ID = i++;
                    item.Name = e1.Current;
                    item.Price = e2.Current;
                    item.MinLevel = e3.Current;
                    item.MinPressure = e4.Current;
                    item.MinTemperature = e5.Current;
                    item.MaxTemperature = e6.Current;
                    list.Add(item);
                }
            }
            var b = GetWritableDatabase();
            b.BeginTransaction();
            foreach (var item in list)
            {
                b.InsertOrReplace(item);
            }
            b.Commit();
            b.Close();
        }

        public void FillLocationFishWeightMult()
        {
            int i = 0;
            var list = new List<LocationFishWeightMult>();

            for (int j = 0; j < 18; j++)
            {
                var fishNodes = (from XmlNode node in res.ChildNodes
                    where node.Attributes["name"].Value == "loc" + j + "_fishes"
                                 select node).FirstOrDefault()?.ChildNodes;
                var fish = from XmlNode node in fishNodes select Int32.Parse(node.InnerText);
                var weightNodes = (from XmlNode node in res.ChildNodes
                    where node.Attributes["name"].Value == "loc" + j + "_weight_mult"
                                   select node).FirstOrDefault()?.ChildNodes;
                var weight = from XmlNode node in weightNodes select Int32.Parse(node.InnerText);
                using (var e1 = fish.GetEnumerator())
                using (var e2 = weight.GetEnumerator())
                {
                    while (e1.MoveNext() && e2.MoveNext())
                    {
                        var item = new LocationFishWeightMult();
                        item.UID = i++;
                        item.LocationID = j;
                        item.FishID = e1.Current;
                        item.WeightMult = e2.Current;
                        list.Add(item);
                    }
                }
            }

            var b = GetWritableDatabase();
            b.BeginTransaction();
            foreach (var item in list)
            {
                b.InsertOrReplace(item);
            }
            b.Commit();
            b.Close();
        }

        public void FillReel()
        {
            var nameNodes = (from XmlNode node in res.ChildNodes
                             where node.Attributes["name"].Value == "cat_names"
                             select node).FirstOrDefault()?.ChildNodes;
            var names = from XmlNode node in nameNodes select node.InnerText;
            var priceNodes = (from XmlNode node in res.ChildNodes
                              where node.Attributes["name"].Value == "cat_prices"
                              select node).FirstOrDefault()?.ChildNodes;
            var prices = from XmlNode node in priceNodes select Int32.Parse(node.InnerText);
            var chanceNodes = (from XmlNode node in res.ChildNodes
                               where node.Attributes["name"].Value == "cat_props"
                               select node).FirstOrDefault()?.ChildNodes;
            var chance = from XmlNode node in chanceNodes select Int32.Parse(node.InnerText);
            var list = new List<Reel>();
            int i = 0;
            using (var e1 = names.GetEnumerator())
            using (var e2 = prices.GetEnumerator())
            using (var e3 = chance.GetEnumerator())
            {
                while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext())
                {
                    var item = new Reel();
                    item.ID = i++;
                    item.Name = e1.Current;
                    item.Price = e2.Current;
                    item.Chance = e3.Current;
                    list.Add(item);
                }
            }
            var b = GetWritableDatabase();
            b.BeginTransaction();
            foreach (var item in list)
            {
                b.InsertOrReplace(item);
            }
            b.Commit();
            b.Close();
        }

        public void FillRod()
        {
            var nameNodes = (from XmlNode node in res.ChildNodes
                             where node.Attributes["name"].Value == "ud_names"
                             select node).FirstOrDefault()?.ChildNodes;
            var names = from XmlNode node in nameNodes select node.InnerText;
            var priceNodes = (from XmlNode node in res.ChildNodes
                              where node.Attributes["name"].Value == "ud_prices"
                              select node).FirstOrDefault()?.ChildNodes;
            var prices = from XmlNode node in priceNodes select Int32.Parse(node.InnerText);
            var weightNodes = (from XmlNode node in res.ChildNodes
                               where node.Attributes["name"].Value == "ud_props"
                               select node).FirstOrDefault()?.ChildNodes;
            var weight = from XmlNode node in weightNodes select Int32.Parse(node.InnerText);
            var list = new List<Rod>();
            int i = 0;
            using (var e1 = names.GetEnumerator())
            using (var e2 = prices.GetEnumerator())
            using (var e3 = weight.GetEnumerator())
            {
                while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext())
                {
                    var item = new Rod();
                    item.ID = i++;
                    item.Name = e1.Current;
                    item.Price = e2.Current;
                    item.MaxWeight = e3.Current;
                    list.Add(item);
                }
            }
            var b = GetWritableDatabase();
            b.BeginTransaction();
            foreach (var item in list)
            {
                b.InsertOrReplace(item);
            }
            b.Commit();
            b.Close();
        }

        public void FillSpinning()
        {
            var nameNodes = (from XmlNode node in res.ChildNodes
                             where node.Attributes["name"].Value == "ud_spin_names"
                             select node).FirstOrDefault()?.ChildNodes;
            var names = from XmlNode node in nameNodes select node.InnerText;
            var priceNodes = (from XmlNode node in res.ChildNodes
                              where node.Attributes["name"].Value == "ud_spin_prices"
                              select node).FirstOrDefault()?.ChildNodes;
            var prices = from XmlNode node in priceNodes select Int32.Parse(node.InnerText);
            var weightNodes = (from XmlNode node in res.ChildNodes
                               where node.Attributes["name"].Value == "ud_spin_props"
                               select node).FirstOrDefault()?.ChildNodes;
            var weight = from XmlNode node in weightNodes select Int32.Parse(node.InnerText);
            var list = new List<Spinning>();
            int i = 0;
            using (var e1 = names.GetEnumerator())
            using (var e2 = prices.GetEnumerator())
            using (var e3 = weight.GetEnumerator())
            {
                while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext())
                {
                    var item = new Spinning();
                    item.ID = i++;
                    item.Name = e1.Current;
                    item.Price = e2.Current;
                    item.MaxWeight = e3.Current;
                    list.Add(item);
                }
            }
            var b = GetWritableDatabase();
            b.BeginTransaction();
            foreach (var item in list)
            {
                b.InsertOrReplace(item);
            }
            b.Commit();
            b.Close();
        }

        public void FillSpinningLure()
        {
            var nameNodes = (from XmlNode node in res.ChildNodes
                             where node.Attributes["name"].Value == "spin_names"
                             select node).FirstOrDefault()?.ChildNodes;
            var names = from XmlNode node in nameNodes select node.InnerText;
            var priceNodes = (from XmlNode node in res.ChildNodes
                              where node.Attributes["name"].Value == "spin_prices"
                              select node).FirstOrDefault()?.ChildNodes;
            var prices = from XmlNode node in priceNodes select Int32.Parse(node.InnerText);
            var codeNodes = (from XmlNode node in res.ChildNodes
                               where node.Attributes["name"].Value == "spin_signs"
                               select node).FirstOrDefault()?.ChildNodes;
            var code = from XmlNode node in codeNodes select node.InnerText;
            var list = new List<SpinningLure>();
            int i = 0;
            using (var e1 = names.GetEnumerator())
            using (var e2 = prices.GetEnumerator())
            using (var e3 = code.GetEnumerator())
            {
                while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext())
                {
                    var item = new SpinningLure();
                    item.ID = i++;
                    item.Name = e1.Current;
                    item.Price = e2.Current;
                    item.Code = e3.Current;
                    item.Chance = 2;
                    list.Add(item);
                }
            }
            var b = GetWritableDatabase();
            b.BeginTransaction();
            foreach (var item in list)
            {
                b.InsertOrReplace(item);
            }
            b.Commit();
            b.Close();
        }

        public void FillSpinningLureSpeed()
        {
            var speedNodes = (from XmlNode node in res.ChildNodes
                             where node.Attributes["name"].Value == "spin_speeds"
                              select node).FirstOrDefault()?.ChildNodes;
            var speed = from XmlNode node in speedNodes select node.InnerText;
            var list = new List<SpinningLureSpeed>();
            int i = 0;
            using (var e1 = speed.GetEnumerator())
            {
                while (e1.MoveNext())
                {
                    var item = new SpinningLureSpeed();
                    item.ID = i++;
                    item.FastAllowed = e1.Current.ToLower().Contains("fast");
                    item.MediumAllowed = e1.Current.ToLower().Contains("medium");
                    item.SlowAllowed = e1.Current.ToLower().Contains("slow");
                    list.Add(item);
                }
            }
            var b = GetWritableDatabase();
            b.BeginTransaction();
            foreach (var item in list)
            {
                b.InsertOrReplace(item);
            }
            b.Commit();
            b.Close();
        }
    }
}
