﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using DataModels;
using SQLite;

namespace ConvertDB
{
    class Program
    {
        static void Main(string[] args)
        {
            var db = new DB();
            Console.WriteLine("Creating DB..");
            db.InitDatabase();
            Console.WriteLine("DB created");
            Console.WriteLine("Parsing area lures..");
            db.FillAreaLure();
            Console.WriteLine("Parsing baits..");
            db.FillBait();
            Console.WriteLine("Parsing fishes..");
            db.FillFish();
            db.FillFishAreaLure();
            db.FillFishBaitChance();
            Console.WriteLine("Parsing hooks..");
            db.FillFishingHooks();
            Console.WriteLine("Parsing fishing lines..");
            db.FillFishingLine();
            Console.WriteLine("Parsing fish parameters..");
            db.FillFishSpinningChance();
            db.FillFishSpinningSpeed();
            db.FillFishTimeChance();
            Console.WriteLine("Parsing locations..");
            db.FillLocation();
            db.FillLocationFishWeightMult();
            Console.WriteLine("Parsing reels..");
            db.FillReel();
            Console.WriteLine("Parsing rods..");
            db.FillRod();
            Console.WriteLine("Parsing spinnings..");
            db.FillSpinning();
            Console.WriteLine("Parsing spinning lures..");
            db.FillSpinningLure();
            db.FillSpinningLureSpeed();
        }
    }
}
