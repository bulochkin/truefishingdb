﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace DataModels
{
    public class FishAreaLure
    {
        [NotNull, PrimaryKey, Indexed]
        public int UID { get; set; }
        [NotNull]
        public int AreaLureID { get; set; }
        [NotNull]
        public int FishID { get; set; }
    }
}
