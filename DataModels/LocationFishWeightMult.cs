﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace DataModels
{
    public class LocationFishWeightMult
    {
        [NotNull, PrimaryKey, Indexed]
        public int UID { get; set; }
        [NotNull]
        public int LocationID { get; set; }
        [NotNull]
        public int FishID { get; set; }
        [NotNull]
        public int WeightMult { get; set; }
    }
}
