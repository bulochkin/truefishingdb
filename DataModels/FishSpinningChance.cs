﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace DataModels
{
    public class FishSpinningChance
    {
        [NotNull, PrimaryKey, Indexed]
        public int UID { get; set; }
        [NotNull]
        public int SpinningLureID { get; set; }
        [NotNull]
        public int FishID { get; set; }
        [NotNull]
        public int Chance { get; set; }
        [NotNull]
        public int MinWeight { get; set; }
        [NotNull]
        public int MaxWeight { get; set; }
    }
}
