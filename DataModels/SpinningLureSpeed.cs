﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace DataModels
{
    public class SpinningLureSpeed
    {
        [NotNull, PrimaryKey, Indexed]
        public int ID { get; set; }
        [NotNull]
        public bool SlowAllowed { get; set; }
        [NotNull]
        public bool MediumAllowed { get; set; }
        [NotNull]
        public bool FastAllowed { get; set; }
    }
}
