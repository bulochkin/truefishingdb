﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace DataModels
{
    public class Location
    {
        [NotNull, PrimaryKey, Indexed]
        public int ID { get; set; }
        [NotNull]
        public String Name { get; set; }
        [NotNull]
        public int Price { get; set; }
        [NotNull]
        public int MinLevel { get; set; }
        [NotNull]
        public int MinPressure { get; set; }
        [NotNull]
        public int MinTemperature { get; set; }
        [NotNull]
        public int MaxTemperature { get; set; }
    }
}
