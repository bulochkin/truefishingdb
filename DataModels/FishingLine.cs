﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace DataModels
{
    public class FishingLine
    {
        [NotNull, PrimaryKey, Indexed]
        public int ID { get; set; }
        [NotNull]
        public String Name { get; set; }
        [NotNull]
        public int Price { get; set; }
        [NotNull]
        public int MaxWeight { get; set; }
    }
}
