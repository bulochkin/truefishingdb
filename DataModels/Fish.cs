﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace DataModels
{
    public class Fish
    {
        [NotNull, PrimaryKey, Indexed]
        public int ID { get; set; }
        [NotNull]
        public String Name { get; set; }
        [NotNull]
        public int Price { get; set; }
        [NotNull]
        public int Pressure { get; set; }
        [NotNull]
        public int DepthMin { get; set; }
        [NotNull]
        public int DepthMax { get; set; }
        [NotNull]
        public int MinTemperature { get; set; }
        [NotNull]
        public int Expirience { get; set; }
        [NotNull]
        public int MinTrophyWeight { get; set; }
    }
}
