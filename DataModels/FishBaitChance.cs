﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace DataModels
{
    public class FishBaitChance
    {
        [NotNull, PrimaryKey, Indexed]
        public int UID { get; set; }
        [NotNull]
        public int LureID { get; set; }
        [NotNull]
        public int FishID { get; set; }
        [NotNull]
        public int LureChance { get; set; }
        [NotNull]
        public int LureMinWeight { get; set; }
        [NotNull]
        public int LureMaxWeight { get; set; }
    }
}
