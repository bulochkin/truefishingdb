﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace DataModels
{
    public class FishTimeChance
    {
        [NotNull, PrimaryKey, Indexed]
        public int ID { get; set; }
        [NotNull]
        public String TimeChance { get; set; }
    }
}
