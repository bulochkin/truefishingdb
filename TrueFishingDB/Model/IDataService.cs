﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModels;
using TrueFishingDB.ViewModel;

namespace TrueFishingDB.Model
{
    public interface IDataService
    {
        void GetFishes(Action<Fish, Exception> callback);
        void GetLocationsForFish(Fish fish, Action<LocationViewModel, Exception> callback);
        void GetBaitsForFish(Fish fish, Action<BaitViewModel, Exception> callback);
        void GetAreaLuresForFish(Fish fish, Action<AreaLure, Exception> callback);
        void GetSpinningLuresForFish(Fish fish, Action<SpinningLureViewModel, Exception> callback);
    }
}
