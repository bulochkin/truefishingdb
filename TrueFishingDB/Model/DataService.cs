﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Animation;
using DataModels;
using SQLite;
using TrueFishingDB.ViewModel;

namespace TrueFishingDB.Model
{
    public class DataService : IDataService
    {
        private SQLiteConnection GetReadableDatabase()
        {
            return new SQLiteConnection("db.db", SQLiteOpenFlags.ReadOnly | SQLiteOpenFlags.NoMutex | SQLiteOpenFlags.ProtectionNone | SQLiteOpenFlags.SharedCache);
        }

        public void GetFishes(Action<Fish, Exception> callback)
        {
            using (var db = GetReadableDatabase())
            {
                var fishes = db.Table<Fish>();
                foreach (var fish in fishes)
                {
                    callback(fish, null);
                }
            }
        }

        public void GetLocationsForFish(Fish fish, Action<LocationViewModel, Exception> callback)
        {
            using (var db = GetReadableDatabase())
            {
                var locweight = db.Table<LocationFishWeightMult>();
                var locs = db.Table<Location>();
                var ids = from LocationFishWeightMult it in locweight where it.FishID == fish.ID select it;
                foreach (var locationFishWeightMult in ids)
                {
                    var l = locs.FirstOrDefault(i => i.ID == locationFishWeightMult.LocationID);
                    if (l == null)
                        continue;
                    var loc = new LocationViewModel(l);
                    loc.WeightMult = locationFishWeightMult.WeightMult.ToString();
                    callback(loc, null);
                }
            }
        }

        public void GetBaitsForFish(Fish fish, Action<BaitViewModel, Exception> callback)
        {
            using (var db = GetReadableDatabase())
            {
                var baitChances = db.Table<FishBaitChance>();
                var baits = db.Table<Bait>();
                var ids = from FishBaitChance it in baitChances where it.FishID == fish.ID select it;
                foreach (var baitChance in ids)
                {
                    var b = baits.FirstOrDefault(i => i.ID == baitChance.LureID);
                    if (b == null)
                        continue;
                    var bait = new BaitViewModel(b, baitChance);
                    
                    callback(bait, null);
                }
            }
        }

        public void GetAreaLuresForFish(Fish fish, Action<AreaLure, Exception> callback)
        {
            using (var db = GetReadableDatabase())
            {
                var lures = db.Table<AreaLure>();
                var fishLures = db.Table<FishAreaLure>();
                var ids = from FishAreaLure it in fishLures where it.FishID == fish.ID select it;
                foreach (var fishLure in ids)
                {
                    var lure = lures.FirstOrDefault(i => i.ID == fishLure.AreaLureID);
                    if (lure == null)
                        continue;
                    lure.Price /= lure.StackCount;
                    callback(lure, null);
                }
            }
        }

        public void GetSpinningLuresForFish(Fish fish, Action<SpinningLureViewModel, Exception> callback)
        {
            using (var db = GetReadableDatabase())
            {
                FishSpinningSpeed fishSpinningSpeed = db.Table<FishSpinningSpeed>().FirstOrDefault(i => i.ID == fish.ID);
                var lures = db.Table<SpinningLure>();
                var chances = from FishSpinningChance ch in db.Table<FishSpinningChance>() where ch.FishID == fish.ID select ch;
                var lureSpeeds = db.Table<SpinningLureSpeed>();
                using (var e1 = chances.GetEnumerator())
                {
                    while (e1.MoveNext())
                    {
                        var lure = lures.FirstOrDefault(i => i.ID == e1.Current.SpinningLureID);
                        if (lure == null)
                            continue;
                        var lureSpeed = lureSpeeds.FirstOrDefault(i => i.ID == e1.Current.SpinningLureID);
                        callback(new SpinningLureViewModel(lure, e1.Current, fishSpinningSpeed, lureSpeed), null);
                    }
                }
            }
        }
    }
}