﻿using System;
using DataModels;
using TrueFishingDB.Model;
using TrueFishingDB.ViewModel;

namespace TrueFishingDB.Design
{
    public class DesignDataService : IDataService
    {

        public void GetFishes(Action<Fish, Exception> callback)
        {
            var item = new Fish();
            item.Name = "Карась";
            callback(item, null);
            item = new Fish();
            item.Name = "Карась золотой";
            callback(item, null);
        }

        public void GetLocationsForFish(Fish fish, Action<LocationViewModel, Exception> callback)
        {
            var loc = new LocationViewModel(new Location() {Name = "Карасевый пруд", MinLevel = 0, Price = 0});
            loc.WeightMult = "100";
            callback(loc, null);
        }

        public void GetBaitsForFish(Fish fish, Action<BaitViewModel, Exception> callback)
        {
            var bait = new BaitViewModel(new Bait() { Name = "Червяк", Price = 100, CountInStack = 10}, new FishBaitChance(){LureChance = 100, LureMinWeight = 1000, LureMaxWeight = 3000});
            callback(bait, null);
        }

        public void GetAreaLuresForFish(Fish fish, Action<AreaLure, Exception> callback)
        {
            var lure = new AreaLure() {Name = "Анисовое масло", Price = 100, StackCount = 10};
            callback(lure, null);
        }

        public void GetSpinningLuresForFish(Fish fish, Action<SpinningLureViewModel, Exception> callback)
        {
            var sp = new SpinningLureViewModel(new SpinningLure(), new FishSpinningChance(), new FishSpinningSpeed(), new SpinningLureSpeed());
            callback(sp, null);
        }
    }
}