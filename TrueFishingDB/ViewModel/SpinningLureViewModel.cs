﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModels;
using GalaSoft.MvvmLight;

namespace TrueFishingDB.ViewModel
{
    public class SpinningLureViewModel : ViewModelBase
    {
        private SpinningLure _lure;
        private FishSpinningChance _chance;
        private FishSpinningSpeed _fishSpeed;
        private SpinningLureSpeed _speed;

        public const string NamePropertyName = "Name";
        public string Name
        {
            get
            {
                return _lure.Name;
            }
        }

        public const string PricePropertyName = "Price";
        public string Price
        {
            get
            {
                return _lure.Price.ToString();
            }
        }

        public const string ChancePropertyName = "Chance";
        public string Chance
        {
            get
            {
                return _chance.Chance.ToString();
            }
        }

        public const string MinWeightPropertyName = "MinWeight";
        public string MinWeight
        {
            get
            {
                return _chance.MinWeight.ToString();
            }
        }

        public const string MaxWeightPropertyName = "MaxWeight";
        public string MaxWeight
        {
            get
            {
                return _chance.MaxWeight.ToString();
            }
        }

        public const string SpeedPropertyName = "Speed";
        public string Speed
        {
            get
            {
                StringBuilder res = new StringBuilder();
                if (_speed.SlowAllowed && _fishSpeed.SlowAllowed)
                    res.Append("slow");
                if (_speed.MediumAllowed && _fishSpeed.MediumAllowed)
                {
                    if (res.Length != 0)
                        res.Append(", ");
                    res.Append("medium");
                }
                if (_speed.FastAllowed && _fishSpeed.FastAllowed)
                {
                    if (res.Length != 0)
                        res.Append(", ");
                    res.Append("fast");
                }
                return res.ToString();
            }
        }

        public SpinningLureViewModel(SpinningLure lure, FishSpinningChance chance, FishSpinningSpeed fishSpeed, SpinningLureSpeed speed)
        {
            _lure = lure;
            _chance = chance;
            _speed = speed;
            _fishSpeed = fishSpeed;
        }
    }
}
