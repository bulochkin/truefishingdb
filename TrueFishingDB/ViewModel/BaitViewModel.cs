﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModels;
using GalaSoft.MvvmLight;

namespace TrueFishingDB.ViewModel
{
    public class BaitViewModel : ViewModelBase
    {
        private Bait _bait;
        private FishBaitChance _chance;

        public const string NamePropertyName = "Name";
        public string Name
        {
            get
            {
                return _bait.Name;
            }
        }

        public const string PricePropertyName = "Price";
        public string Price
        {
            get
            {
                return (_bait.Price / _bait.CountInStack).ToString();
            }
        }

        public const string ChancePropertyName = "Chance";
        public string Chance
        {
            get
            {
                return _chance.LureChance.ToString();
            }
        }

        public const string MinWeightPropertyName = "MinWeight";
        public string MinWeight
        {
            get
            {
                return _chance.LureMinWeight.ToString();
            }
        }

        public const string MaxWeightPropertyName = "MaxWeight";
        public string MaxWeight
        {
            get
            {
                return _chance.LureMaxWeight.ToString();
            }
        }

        public BaitViewModel(Bait bait, FishBaitChance chance)
        {
            _bait = bait;
            _chance = chance;
        }
    }
}
