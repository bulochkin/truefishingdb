﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModels;
using GalaSoft.MvvmLight;

namespace TrueFishingDB.ViewModel
{
    public class LocationViewModel : ViewModelBase
    {
        private Location _location;

        public const string NamePropertyName = "Name";
        public string Name
        {
            get
            {
                return _location.Name;
            }
        }

        public const string PricePropertyName = "Price";
        public string Price
        {
            get
            {
                return _location.Price.ToString();
            }
        }

        public const string MinLevelPropertyName = "MinLevel";
        public string MinLevel
        {
            get
            {
                return _location.MinLevel.ToString();
            }
        }

        private String _weightMult;
        public const string WeightMultPropertyName = "WeightMult";
        public string WeightMult
        {
            get
            {
                return _weightMult;
            }
            set { Set(ref _weightMult, value); }
        }

        public LocationViewModel(Location loc)
        {
            _location = loc;
        }
    }
}
