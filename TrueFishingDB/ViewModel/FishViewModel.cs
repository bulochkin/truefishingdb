﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using DataModels;
using GalaSoft.MvvmLight;
using TrueFishingDB.Model;

namespace TrueFishingDB.ViewModel
{
    class FishViewModel : ViewModelBase
    {
        private Fish _fishToShow;

        public const string NamePropertyName = "Name";
        public string Name
        {
            get
            {
                return _fishToShow.Name;
            }
        }

        public const string PricePropertyName = "Price";
        public string Price
        {
            get
            {
                return _fishToShow.Price.ToString();
            }
        }

        public const string MinPressurePropertyName = "MinPressure";
        public string MinPressure
        {
            get
            {
                return _fishToShow.Pressure.ToString();
            }
        }

        public const string MinDepthPropertyName = "MinDepth";
        public string MinDepth
        {
            get
            {
                return _fishToShow.DepthMin.ToString();
            }
        }

        public const string MaxDepthPropertyName = "MaxDepth";
        public string MaxDepth
        {
            get
            {
                return _fishToShow.DepthMax.ToString();
            }
        }

        public const string MinTempPropertyName = "MinTemp";
        public string MinTemp
        {
            get
            {
                return _fishToShow.MinTemperature.ToString();
            }
        }

        public const string ExpiriencePropertyName = "Expirience";
        public string Expirience
        {
            get
            {
                return _fishToShow.Expirience.ToString();
            }
        }

        public const string MinTrophyWeightPropertyName = "MinTrophyWeight";
        public string MinTrophyWeight
        {
            get
            {
                return _fishToShow.MinTrophyWeight.ToString();
            }
        }

        public const String LocationsPropertyName = "Locations";
        private ObservableCollection<LocationViewModel> _locatins = new ObservableCollection<LocationViewModel>();
        public ObservableCollection<LocationViewModel> Locations
        {
            get { return _locatins; }
            private set { Set(ref _locatins, value); }
        }

        public const String BaitsPropertyName = "Baits";
        private ObservableCollection<BaitViewModel> _baits = new ObservableCollection<BaitViewModel>();
        public ObservableCollection<BaitViewModel> Baits
        {
            get { return _baits; }
            private set { Set(ref _baits, value); }
        }

        public const String AreaLuresPropertyName = "AreaLures";
        private ObservableCollection<AreaLure> _areaLures = new ObservableCollection<AreaLure>();
        public ObservableCollection<AreaLure> AreaLures
        {
            get { return _areaLures; }
            private set { Set(ref _areaLures, value); }
        }

        public const String SpinnungLuresPropertyName = "SpinningLures";
        private ObservableCollection<SpinningLureViewModel> _spinningLures = new ObservableCollection<SpinningLureViewModel>();
        public ObservableCollection<SpinningLureViewModel> SpinningLures
        {
            get { return _spinningLures; }
            private set { Set(ref _spinningLures, value); }
        }

        public FishViewModel(IDataService dataService, Fish fish)
        {
            _fishToShow = fish;
            dataService.GetLocationsForFish(fish,
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }

                    _locatins.Add(item);
                });
            dataService.GetBaitsForFish(fish,
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }

                    _baits.Add(item);
                });
            dataService.GetAreaLuresForFish(fish,
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }

                    _areaLures.Add(item);
                });
            dataService.GetSpinningLuresForFish(fish,
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }

                    _spinningLures.Add(item);
                });
        }
    }
}
