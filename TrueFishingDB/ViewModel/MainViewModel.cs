﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using GalaSoft.MvvmLight;
using TrueFishingDB.Model;
using DataModels;
using GalaSoft.MvvmLight.CommandWpf;
using TrueFishingDB.UserControls;

namespace TrueFishingDB.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.mvvmlight.net
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly IDataService _dataService;

        /// <summary>
        /// The <see cref="WelcomeTitle" /> property's name.
        /// </summary>
        public const string WelcomeTitlePropertyName = "WelcomeTitle";

        private string _welcomeTitle = string.Empty;

        /// <summary>
        /// Gets the WelcomeTitle property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string WelcomeTitle
        {
            get
            {
                return _welcomeTitle;
            }
            set
            {
                Set(ref _welcomeTitle, value);
            }
        }

        /// <summary>
        /// The <see cref="Fishes" /> property's name.
        /// </summary>
        public const string FishesPropertyName = "Fishes";

        private ObservableCollection<Fish> _fishes = new ObservableCollection<Fish>();

        /// <summary>
        /// Gets the Fishes property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<Fish> Fishes
        {
            get
            {
                return _fishes;
            }
            set
            {
                Set(ref _fishes, value);
            }
        }

        /// <summary>
        /// The <see cref="ShowFishCommand" /> property's name.
        /// </summary>
        public const string ShowFishCommandName = "ShowFishCommand";

        private RelayCommand<Fish> _showFishCommand;

        /// <summary>
        /// Gets the Fishes property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public RelayCommand<Fish> ShowFishCommand
        {
            get
            {
                return _showFishCommand;
            }
            private set
            {
                Set(ref _showFishCommand, value);
            }
        }


        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IDataService dataService)
        {
            _dataService = dataService;
            _dataService.GetFishes(
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }

                    _fishes.Add(item);
                });
            _showFishCommand = new RelayCommand<Fish>(this.ShowFish, f => true);
        }

        private void ShowFish(Fish f)
        {
            FishViewModel vm = new FishViewModel(_dataService, f);
            Window w = new Window() {Title = f.Name, Content = new FishView() {DataContext = vm}, SizeToContent = SizeToContent.WidthAndHeight};
            w.Show();
        }

        ////public override void Cleanup()
        ////{
        ////    // Clean up if needed

        ////    base.Cleanup();
        ////}
    }
}